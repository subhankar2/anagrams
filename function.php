<?php 
	require_once('json-machine-master/vendor/autoload.php'); 
	use \JsonMachine\JsonMachine;

	$time_start = microtime(true); 
	function isAnagram($string1, $string2)
	{
	   if (count_chars($string1, 1) == count_chars($string2, 1)){
	   		return true;
        }else{
        	return false;
        }
	}

	$response = [];
	if(isset($_POST['word'])){
		$input = $_POST['word'];
		$fruits = JsonMachine::fromFile('words_dictionary.json');
		//print_r($fruits);
		$result = [];
		//echo $input;
		foreach ($fruits as $name => $data) {
			if(isAnagram($input, $name)){
				array_push($result, $name);
			}
		}

		$response['data'] = implode(', ',$result);
		$response['status'] = true;
	}else{
		$response['message'] = 'Not Found';
		$response['status'] = false;
	}
	$time_end = microtime(true);

	//dividing with 60 will give the execution time in minutes otherwise seconds
	$execution_time = ($time_end - $time_start)/60;

	$response['time'] = $execution_time;

	echo json_encode($response);

	
	

?>