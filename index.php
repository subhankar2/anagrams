<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Anagrams</title>
</head>
<body>
	<?php 
		$serverUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 
	?>
	<form action="<?php echo $serverUrl.'function.php'; ?>" method="post">
		<input type="text" name="word" value="" id="inputWord" placeholder="Type a word ..." required>
		<button type="button" id="submitBtn">Get Result</button>
	</form>
	<h2>Results - </h2>
	<div id="resultArea"> 
		

	</div>

	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
	<script type="text/javascript">
		$('#submitBtn').on('click', function(){
			var inputWord = $('#inputWord').val();

			var url = $(this).closest('form').attr('action');
			//alert(url);
			if(inputWord !=""){
				$('#resultArea').html('Please Wait...');
				//alert(inputWord);
				$.post(url, {word: inputWord}, function(result){
					let data = JSON.parse(result);
					if(data.status == true){
						$('#resultArea').html(data.data);
						$('#resultArea').append('<br/>Time - '+data.time);
					}else{
						$('#resultArea').html(data.message);
						$('#resultArea').append('<br/>Time - '+data.time);
					}
				});
			}else{
				alert('Please input a word');
			}
			
			
		});
		

	</script>
</body>
</html>